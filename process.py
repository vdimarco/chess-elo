from numpy import mean, std, var, ediff1d, median, percentile
import pandas as pd

# from sklearn.cross_validation import cross_val_score
# from sklearn.ensemble import GradientBoostingRegressor

train = pd.read_csv("data/train.csv")
test = pd.read_csv("data/test.csv")

debug = True

def process(df):
    # df['stockfish'] = map(lambda x: [int(n) for n in x.split(' ')], df['stockfish'].astype(str))
    df['fishsum'] = map(lambda x: sum(x), df['stockfish'])
    df['delta'] = map(lambda x: ediff1d(x), df['stockfish'])
    df['fwhite'] = map(lambda x: [0] if len(x[0::2]) == 0 else x[0::2], df['delta'])
    df['fblack'] = map(lambda x: [0] if len(x[1::2]) == 0 else x[1::2], df['delta'])


    for i in ['white', 'black']:
        df['%s_mean' % i] = map(lambda x: mean(x), df['f%s' % i])
        df['%s_mode' % i] = map(lambda x: median(x), df['f%s' % i])
        df['%s_std' % i] = map(lambda x: std(x), df['f%s' % i])
        df['%s_var' % i] = map(lambda x: var(x), df['f%s' % i])
        df['%s_sum' % i] = map(lambda x: sum(x), df['f%s' % i])
        df['%s_max' % i] = map(lambda x: max(x), df['f%s' % i])
        df['%s_min' % i] = map(lambda x: min(x), df['f%s' % i])
        df['%s_len' % i] = map(lambda x: len(x), df['f%s' % i])
        df['%s_ratio' % i] = map(lambda x: 1.0 * sum(k > 0 for k in x) / len(x), df['f%s' % i])

        df['%s_first' % i] = map(lambda x: x[0], df['f%s' % i])
        df['%s_percentile' % i] = map(lambda x: percentile(x,20), df['f%s' % i])


    df = df.join(pd.get_dummies(df['Result'], prefix='res'))
    df.drop(['stockfish', 'movetext', 'fwhite', 'fblack', 'delta'], 1)
    return df


def split(df):
    if 'WhiteElo' in df.columns:
        white_elo = df['WhiteElo']
        black_elo = df['BlackElo']
        df.drop(['WhiteElo', 'BlackElo'], 1)
        return df.values, white_elo.values, black_elo.values
    else:
        return df.values


train_out = process(train)
test_out = process(test)

print(train_out.head())

train_out.to_csv("data/train_process.csv")
test_out.to_csv("data/test_process.csv")

# model = GradientBoostingRegressor(n_estimators=150, loss='lad', max_depth=5)

# if debug:
#     for i in xrange(0, 3):
#         X_train, whiteElo, blackElo = split(train[train['Result'] == i])
#         scores = cross_val_score(model, X_train, whiteElo, cv=3, scoring='mean_absolute_error')
#         print 'white', scores
#         scores = cross_val_score(model, X_train, blackElo, cv=3, scoring='mean_absolute_error')
#         print 'black', scores

# else:
#     result = None
#     for i in xrange(0, 3):
#         output = {}
#         X_train, whiteElo, blackElo = split(train[train['Result'] == i])
#         X_test = split(test[test['Result'] == i])

#         model = model.fit(X_train, whiteElo)
#         output['WhiteElo'] = model.predict(X_test).astype(int)

#         model = model.fit(X_train, blackElo)
#         output['BlackElo'] = model.predict(X_test).astype(int)

#         if result is None:
#             result = pd.DataFrame(output, index=test[test['Result'] == i].index, columns=['WhiteElo', 'BlackElo'])
#         else:
#             result = result.append(
#                 pd.DataFrame(output, index=test[test['Result'] == i].index, columns=['WhiteElo', 'BlackElo']))

#     result.to_csv('result.csv')s