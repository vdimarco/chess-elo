Features
- stockfish move strength
	- mean, std, etc? for each white & black. 
- result
- game length
- opening
	- attack (white)
	- defense (black)
- time to castling
- agression
	- number of takes
- cumulative material advantage
- positional play ?
	- tbd
- tactical play ? 
	- tbd


Possible Scenarios
- Equal, Not Equal
- High ELO, Low ELO (are these all masters games?)


Outlier Games
- 23782, en prise pawns
- 42887, en prise queen by knight, en prise rook by knight