# elos=`grep "Elo" data.pgn | cut -f2 -d'"'`
# sorted_list=`echo $elos | sort`
# median_idx=`grep "WhiteElo" data.pgn | wc -l`
# median=`echo $sorted_list | tail -n $median_idx | head -n 1`
# submission_ids=`seq 25001 50000`
# row=`yes $median | head -n 2 | paste -sd, -`
# columns=`yes $row | head -n 25000`
# submission=`paste -d, <(echo $submission_ids) <(echo $columns)`
# { echo "Event,WhiteElo,BlackElo";echo $submission} > submission.csv

{ echo "Event,WhiteElo,BlackElo";paste -d, <(seq 25001 50000) <(yes $(yes $(grep "Elo" data.pgn | cut -f2 -d'"' | sort | tail -n $(grep "WhiteElo" data.pgn | wc -l) | head -n 1) | head -n 2 | paste -sd, -) | head -n 25000); } > submission.csv