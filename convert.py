import re
import pandas

data = open('data/raw/data.pgn').readlines()
stock = [x.split(',') for x in open('data/raw/stockfish.csv').readlines()]

test, train = [], []
nl_count = 0
buffer = ''

# Draw - 0, White - 1, Black -2
replacements = {'1/2-1/2': '0', '1-0': '1', '0-1': '2'}

for line in data:
    buffer += line
    if line == '\n':
        nl_count += 1
        if nl_count == 2:
            game = {}

            for name, value in re.findall('\[([a-zA-Z]*?) "(.*?)"\]', buffer):
                game[name] = value

            id = int(game['Event'])

            movetext = buffer.split('\n\n')[1]
            movetext = movetext[0:movetext.rfind('\n')].replace('\n', ' ')
            movetext = re.split(' {0,1}[0-9]{1,3}\. ', movetext)
            game['movetext'] = ';'.join(movetext[1:])
            game['stockfish'] = stock[id][1].strip().replace('NA', '0')
            if len(game['stockfish']) == 0:
                game['stockfish'] = '0'
            for k, v in replacements.iteritems():
                game['Result'] = game['Result'].replace(k, v)

            buffer, nl_count = '', 0

            if id <= 25000:
                train.append(game)
            else:
                test.append(game)

train = pandas.DataFrame(train)
test = pandas.DataFrame(test)
train_columns = ['Event', 'Result', 'movetext', 'stockfish', 'WhiteElo', 'BlackElo']
test_columns = ['Event', 'Result', 'movetext', 'stockfish']
train = train[train_columns]
test = test[test_columns]
train.to_csv('data/train.csv', index=False)
test.to_csv('data/test.csv', index=False)